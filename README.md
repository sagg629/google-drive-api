Implementación del API de Google Drive, extraído del ejemplo oficial.

Quickstart / Autenticación: https://developers.google.com/drive/v3/web/quickstart/js

Picker: https://developers.google.com/picker/docs/

No olvidar configurar en la consola de Google Developers, el origin "http://localhost:8000"

Ejecutar este proyecto: python -m SimpleHTTPServer 8000
